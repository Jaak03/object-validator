const YAML = require('yamljs');
const { default: objectValidator } = require('./lib/index');

(async () => {
  const testValidationRules = YAML.load('./validationRules.yml');
  console.log(testValidationRules);
})();

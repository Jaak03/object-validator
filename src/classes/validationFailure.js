/**
 * Class that transports details about the validation rule that failed.
 */
export default class ValidationFailure {
  constructor(rule, value) {
    
  }

  setValue(value) {
    this.value = value;
  }

  setMessageBundle() {

  }
}


/**
 * Checks whether the field that was sent to the function satisfies the multiplicity rule.
 * 
 * Options for the multiplicity rule:
 *  - LowerBound: Should be >=0 to indicate a minimum number of occurrences that is allowed.
 *  - UpperBound:
 * 
 *      "{lowerBound}..{upperBound}": string
 * 
 * Examples:
 *  - *"0..1"*: An optional field that can either be 0 or 1.
 *  - *"5..*"*: An array with a minimum of 5 items.
 *  - *"1"*:    A compulsory field that has to be present.
 * 
 * @param {any} field to be checked for multiplicity.
 * @param {string} rule defining what multiplicity it should satisfy.
 * @returns [ValidationError]
 */
function multiplicity(field, rule) {
  const [lowerBound, upperBound] = rule.split('..');
  
  if (lowerBound === '*')

  return 'Checking if the multiplicity works.';
}

export default multiplicity;

import stringValidator from './string';

const validators = {
  string: stringValidator,
};

export default validators;
